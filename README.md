# FSR-API

## It's time to develop ! 👨‍💻

FSR is a web application designed to simplify the way you manage your contacts and stay organized. Whether it's adding, modifying, or deleting contacts, or efficiently organizing them into customized groups, FSR offers a seamless and user-friendly experience. Built using the power of Spring and Hibernate.
## Getting started  🏎

To make it easy for you to get started with this project, here's a list of recommended next steps.

- [INSTALL WSL DEBIAN](https://www.linuxfordevices.com/tutorials/linux/install-debian-on-windows-wsl)
- [INSTALL DOCKER](https://docs.docker.com/engine/install/debian/#install-from-a-package)
- [INSTALL MAKE](https://installati.one/debian/11/make/)
- [INSTALL INTELLIJ DEA](https://www.jetbrains.com/help/idea/installation-guide.html)

## Clone project  🧬

    Create this tree project and clone the project : 

```
    - cd && mkdir fsr-project && cd fsr-project && git clone https://gitlab.com/fsr-project/fsr-api.git
```

## Define git global information    👀

    Change these lines, adding your personal informations (git information): 

```    
    git config --global user.email "gitlab-email" \
         &&  git config --global user.name "gitlab-name"

```

## Configure DataBase   🛢️👀

    Create a local file for the .env and change the databse url

```
    cp .env .env.local
    
    inside the file replace the database url with this : 
    
        DATABASE_URL="postgresql://root:ChangeMe@database:5432/app?serverVersion=15&charset=utf8"
    
```
## Start project    🏁
#### Always run these commands to start the project : 

```
    sudo service docker start
```
AND

```
    make install
    make start
```