install:
	docker compose -f ./docker/docker-compose.yml up -d --build
	docker exec -it fsr-api-container mvn clean install
.PHONY: install

kill:
	docker system prune -a
	docker kill fsr-api-container docker-database-1
	docker rm fsr-api-container docker-database-1
	docker images
.PHONY: kill

start:
	docker exec -it fsr-api-container mvn tomcat7:run
.PHONY: start

stop:
	docker exec -it fsr-api-container mvn tomcat7:stop
.PHONY: stop

reset-db:
	docker exec -it fsr-api-container chmod +x bin/console
.PHONY: exec

exec:
	docker exec -it fsr-api-container bash
.PHONY: exec