USE `app`;

-- Inserting addresses for the contacts
INSERT INTO addresses(city, country, street, zip) VALUES ('City1', 'Country1', 'Street1', 'Zip1');
INSERT INTO addresses(city, country, street, zip) VALUES ('City2', 'Country2', 'Street2', 'Zip2');
INSERT INTO addresses(city, country, street, zip) VALUES ('City3', 'Country3', 'Street3', 'Zip3');
INSERT INTO addresses(city, country, street, zip) VALUES ('City4', 'Country4', 'Street4', 'Zip4');

-- Inserting contacts with reference to their addresses
-- Assuming the addresses are inserted in the order given, the ids for addresses will start from 1 and increment
INSERT INTO contacts(email, firstname, lastname, idAddress) VALUES ('email1@example.com', 'First1', 'Last1', 1);
INSERT INTO contacts(email, firstname, lastname, idAddress) VALUES ('email2@example.com', 'First2', 'Last2', 2);
INSERT INTO contacts(email, firstname, lastname, idAddress) VALUES ('email3@example.com', 'First3', 'Last3', 3);
INSERT INTO contacts(email, firstname, lastname, idAddress) VALUES ('email4@example.com', 'First4', 'Last4', 4);

-- Inserting phone numbers for the contacts
-- Assuming the contacts are inserted in the order given, the ids for contacts will start from 1 and increment
INSERT INTO phonenumbers(phoneKind, num_tel, idContact) VALUES ('Home', '1234567890', 1);
INSERT INTO phonenumbers(phoneKind, num_tel, idContact) VALUES ('Work', '2345678901', 1);

INSERT INTO phonenumbers(phoneKind, num_tel, idContact) VALUES ('Home', '3456789012', 2);
INSERT INTO phonenumbers(phoneKind, num_tel, idContact) VALUES ('Work', '4567890123', 2);

INSERT INTO phonenumbers(phoneKind, num_tel, idContact) VALUES ('Home', '5678901234', 3);
INSERT INTO phonenumbers(phoneKind, num_tel, idContact) VALUES ('Work', '6789012345', 3);

INSERT INTO phonenumbers(phoneKind, num_tel, idContact) VALUES ('Home', '7890123456', 4);
INSERT INTO phonenumbers(phoneKind, num_tel, idContact) VALUES ('Work', '8901234567', 4);

-- Inserting contact groups
INSERT INTO contactgroups(name) VALUES ('Group1');
INSERT INTO contactgroups(name) VALUES ('Group2');

-- Associating contacts with groups
-- Assuming the groups are inserted in the order given, the ids for groups will start from 1 and increment
INSERT INTO ctc_grp(idContact, idContactGroup) VALUES (1, 1);
INSERT INTO ctc_grp(idContact, idContactGroup) VALUES (2, 1);
INSERT INTO ctc_grp(idContact, idContactGroup) VALUES (3, 1);

INSERT INTO ctc_grp(idContact, idContactGroup) VALUES (2, 2);
INSERT INTO ctc_grp(idContact, idContactGroup) VALUES (3, 2);
INSERT INTO ctc_grp(idContact, idContactGroup) VALUES (4, 2);
