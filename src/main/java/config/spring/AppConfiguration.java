package config.spring;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;


@ImportResource(locations = {"classpath:applicationContext.xml"})
@ComponentScan(basePackages = {"model", "daos", "services", "controller"})
@Configuration
public class AppConfiguration {

}
