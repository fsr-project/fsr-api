package model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "contactgroups")
public class ContactGroup {
    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long idContactGroup;

    @Column(nullable = false)
    private String name;

    @ManyToMany(mappedBy="groups")
    private List<Contact> contacts;

    public ContactGroup(long idContactGroup, String name) {
        this.idContactGroup = idContactGroup;
        this.name = name;
    }

    public ContactGroup(String name, List<Contact> contacts) {
        this.name = name;
        this.contacts = contacts;
    }

    public ContactGroup() {
        this.contacts = new ArrayList<Contact>();
    }

    public void addContact(Contact contact) {
        if (this.contacts == null)
            this.contacts = new ArrayList<>();
        this.contacts.add(contact);
        contact.addGroup(this);
    }

    public long getIdContactGroup() {
        return idContactGroup;
    }

    public void setIdContactGroup(long idContactGroup) {
        this.idContactGroup = idContactGroup;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }
}
