package model;

import javax.persistence.*;

@Entity
@Table(name = "addresses")
public class Address {
	@Id
	@Column(nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idAddress;

	private String street;

	private String city;

	private String zip;

	@Column(length = 50)
	private String country;

	public Address(String street, String city, String zip, String country) {
		this.street = street;
		this.city = city;
		this.zip = zip;
		this.country = country;
	}

	public Address(long idAddress, String street, String city, String zip, String country) {
		this.idAddress = idAddress;
		this.street = street;
		this.city = city;
		this.zip = zip;
		this.country = country;
	}

	public Address() {
	}

	public long getIdAddress() {
		return idAddress;
	}

	public void setIdAddress(long idAddress) {
		this.idAddress = idAddress;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
}
