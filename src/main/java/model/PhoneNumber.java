package model;

import javax.persistence.*;

@Entity
@Table(name = "phonenumbers")
public class PhoneNumber {
    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idPhoneNumber;

    @Column(nullable = false)
    private String phoneKind;

    @Column(name = "num_tel", nullable = false)
    private String phoneNumber;

    public PhoneNumber(String phoneKind, String phoneNumber) {
        this.phoneKind = phoneKind;
        this.phoneNumber = phoneNumber;
    }

    public PhoneNumber(long idPhoneNumber, String phoneKind, String phoneNumber) {
        this.idPhoneNumber = idPhoneNumber;
        this.phoneKind = phoneKind;
        this.phoneNumber = phoneNumber;
    }

    public PhoneNumber() {
    }

    public long getIdPhoneNumber() {
        return idPhoneNumber;
    }

    public void setIdPhoneNumber(long idPhoneNumber) {
        this.idPhoneNumber = idPhoneNumber;
    }

    public String getPhoneKind() {
        return phoneKind;
    }

    public void setPhoneKind(String phoneKind) {
        this.phoneKind = phoneKind;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}