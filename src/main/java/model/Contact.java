package model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@NamedQuery(name = "Contact.getContactById", query = "SELECT c FROM Contact c WHERE c.idContact = :idParam")
@Table(name = "contacts")
public class Contact {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(nullable = false)
    private long idContact;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false)
    private String email;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idAddress", referencedColumnName = "idAddress")
    private Address address;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "idContact")
    private List<PhoneNumber> phoneNumbers;

    @ManyToMany
    @JoinTable(name="ctc_grp",
            joinColumns=@JoinColumn(name="idContact"),
            inverseJoinColumns=@JoinColumn(name="idContactGroup"))
    private List<ContactGroup> groups;

    public Contact(
            String firstName,
            String lastName,
            String email,
            Address address
    ) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.address = address;

        this.phoneNumbers = new ArrayList<>();
        this.groups = new ArrayList<>();
    }

    public void addPhoneNumber(PhoneNumber phoneNumber) {
        if (this.phoneNumbers == null)
            this.phoneNumbers = new ArrayList<>();
        this.phoneNumbers.add(phoneNumber);
    }

    public void addGroup(ContactGroup group) {
        if (this.groups == null)
            this.groups = new ArrayList<>();
        this.groups.add(group);
    }

    public void removeGroup(ContactGroup group) {
        if (this.groups != null && this.groups.size() > 0)
            this.groups.remove(group);
    }

    public Contact() {
    }

    public long getIdContact() {
        return idContact;
    }

    public void setIdContact(long idContact) {
        this.idContact = idContact;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<PhoneNumber> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(List<PhoneNumber> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public List<ContactGroup> getGroups() {
        return groups;
    }

    public void setGroups(List<ContactGroup> groups) {
        this.groups = groups;
    }
}
