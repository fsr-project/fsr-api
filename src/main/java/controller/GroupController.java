package controller;

import controller.dto.creation.GroupCreationDTO;
import controller.dto.detail.group.GroupDetailDTO;
import controller.dto.detail.group.GroupUpdateDTO;
import controller.dto.overview.GroupOverviewDTO;
import daos.exceptions.GroupCreationException;
import daos.exceptions.GroupNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import services.group.IGroupService;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.List;

@Component
@Path("/groups")
public class GroupController {
    @Context
    UriInfo uriInfo;
    @Context
    Request request;

    @Autowired
    private IGroupService groupService;

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_HTML})
    public Response getAllContacts(){
        List<GroupOverviewDTO> groupsDTO = groupService.getAllGroups();
        return Response.ok(groupsDTO).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createGroup(GroupCreationDTO group){
        try{
            groupService.createGroup(group);
            return Response.status(Response.Status.CREATED).entity("Contact created with success").build();
        }catch (GroupCreationException e){
            return Response.status(Response.Status.EXPECTATION_FAILED).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getGroupById(@PathParam("id") long id){
        try {
            GroupDetailDTO group = groupService.getGroupById(id);
            return Response.ok().entity(group).build();
        } catch (GroupNotFoundException exception) {
            return Response.status(Response.Status.NOT_FOUND).entity(exception.getMessage()).build();
        }
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteContact(@PathParam("id") long id){
        try {
            groupService.deleteGroup(id);
            return Response.ok().entity("Contact has been deleted with success").build();
        } catch (GroupNotFoundException exception) {
            return Response.status(Response.Status.NOT_FOUND).entity(exception.getMessage()).build();
        }
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateGroup(GroupUpdateDTO group){
        try {
            groupService.updateGroup(group);
            return Response.ok().entity("Group has been updated with success").build();
        } catch (GroupNotFoundException exception) {
            return Response.status(Response.Status.NOT_FOUND).entity(exception.getMessage()).build();
        }
    }
}
