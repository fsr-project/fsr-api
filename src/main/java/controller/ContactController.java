package controller;

import controller.dto.creation.ContactCreationDTO;
import controller.dto.detail.contact.ContactDetailDTO;
import controller.dto.detail.contact.ContactUpdateDTO;
import controller.dto.overview.ContactOverviewDTO;
import daos.exceptions.ContactCreationException;
import daos.exceptions.ContactNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import services.contact.IContactService;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.List;
import java.util.Optional;

@Component
@Path("/contacts")
public class ContactController {
    @Context
    UriInfo uriInfo;
    @Context
    Request request;
    @Autowired
    private IContactService contactService;

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_HTML})
    public Response getAllContacts(){
        List<ContactOverviewDTO> contactsDTO = contactService.getContacts();
        return Response.ok(contactsDTO).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createContact(ContactCreationDTO contact) {
        try{
            contactService.createContact(contact);
            return Response.status(Response.Status.CREATED).entity("Contact created with success").build();
        }catch (ContactCreationException exception){
            return Response.status(Response.Status.EXPECTATION_FAILED).entity(exception.getMessage()).build();
        }
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getContactById(@PathParam("id") long id){
        try{
            Optional<ContactDetailDTO> contact = contactService.getContactById(id);
            if (contact.isPresent())
                return Response.ok().entity(contact.get()).build();
            return Response.status(Response.Status.BAD_REQUEST).entity("error").build();
        } catch (ContactNotFoundException exception) {
            return Response.status(Response.Status.NOT_FOUND).entity(exception.getMessage()).build();
        }
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteContact(@PathParam("id") long id){
        try{
            contactService.deleteContact(id);
            return Response.ok().entity("Contact has been deleted with success").build();
        } catch (ContactNotFoundException exception) {
            return Response.status(Response.Status.NOT_FOUND).entity(exception.getMessage()).build();
        }
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateContact(ContactUpdateDTO contact){
        try {
            contactService.updateContact(contact);
            return Response.ok().entity("Contact has been updated with success").build();
        } catch (ContactNotFoundException exception) {
            return Response.status(Response.Status.NOT_FOUND).entity(exception.getMessage()).build();
        }
    }

}
