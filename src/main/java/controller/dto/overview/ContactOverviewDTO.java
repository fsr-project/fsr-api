package controller.dto.overview;

import controller.dto.common.CommonContactDTO;

public class ContactOverviewDTO extends CommonContactDTO {
    private long idContact;

    public ContactOverviewDTO(long idContact, String firstName, String lastName, String email) {
        super(firstName, lastName, email);
        this.idContact = idContact;
    }

    public long getIdContact() {
        return idContact;
    }
}
