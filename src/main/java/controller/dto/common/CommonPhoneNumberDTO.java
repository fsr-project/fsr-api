package controller.dto.common;

public class CommonPhoneNumberDTO {
    private String phoneKind;
    private String phoneNumber;

    public CommonPhoneNumberDTO() {
    }

    public CommonPhoneNumberDTO(String phoneKind, String phoneNumber) {
        this.phoneKind = phoneKind;
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneKind() {
        return phoneKind;
    }

    public void setPhoneKind(String phoneKind) {
        this.phoneKind = phoneKind;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
