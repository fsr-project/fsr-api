package controller.dto.common;

public class CommonContactGroupDTO {
    private String name;

    public CommonContactGroupDTO() {
    }

    public CommonContactGroupDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
