package controller.dto.detail.contact;

import controller.dto.common.CommonAddressDTO;

public class AddressDetailDTO extends CommonAddressDTO {
    private long idAddress;

    public AddressDetailDTO() {
    }

    public AddressDetailDTO(
            long idAddress,
            String street,
            String city,
            String zip,
            String country
    ) {
        super(street, city, zip, country);
        this.idAddress = idAddress;
    }

    public long getIdAddress() {
        return idAddress;
    }

    public void setIdAddress(long idAddress) {
        this.idAddress = idAddress;
    }
}
