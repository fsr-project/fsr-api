package controller.dto.detail.contact;

import controller.dto.common.CommonContactDTO;

import java.util.List;

public class ContactDetailDTO extends CommonContactDTO {
    private long idContact;

    private AddressDetailDTO address;

    private List<PhoneNumberDetailDTO> phoneNumbers;

    private List<ContactGroupDetailDTO> groups;

    public ContactDetailDTO(
            long idContact,
            String firstName,
            String lastName,
            String email,
            AddressDetailDTO address,
            List<PhoneNumberDetailDTO> phoneNumbers,
            List<ContactGroupDetailDTO> groups
    ) {
        super(firstName, lastName, email);
        this.idContact = idContact;
        this.address = address;
        this.phoneNumbers = phoneNumbers;
        this.groups = groups;
    }

    public long getIdContact() {
        return idContact;
    }

    public void setIdContact(long idContact) {
        this.idContact = idContact;
    }

    public AddressDetailDTO getAddress() {
        return address;
    }

    public void setAddress(AddressDetailDTO address) {
        this.address = address;
    }

    public List<PhoneNumberDetailDTO> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(List<PhoneNumberDetailDTO> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public List<ContactGroupDetailDTO> getGroups() {
        return groups;
    }

    public void setGroups(List<ContactGroupDetailDTO> groups) {
        this.groups = groups;
    }
}
