package controller.dto.detail.contact;

import controller.dto.common.CommonContactDTO;

import java.util.List;

public class ContactUpdateDTO extends CommonContactDTO {
    private long idContact;

    private AddressDetailDTO address;

    private List<PhoneNumberDetailDTO> phoneNumbers;

    private List<Long> idsContactGroup;

    public ContactUpdateDTO() {
    }

    public ContactUpdateDTO(
            String firstName,
            String lastName,
            String email,
            long idContact,
            AddressDetailDTO address,
            List<PhoneNumberDetailDTO> phoneNumbers,
            List<Long> idsContactGroup
    ) {
        super(firstName, lastName, email);
        this.idContact = idContact;
        this.address = address;
        this.phoneNumbers = phoneNumbers;
        this.idsContactGroup = idsContactGroup;
    }

    public long getIdContact() {
        return idContact;
    }

    public void setIdContact(long idContact) {
        this.idContact = idContact;
    }

    public AddressDetailDTO getAddress() {
        return address;
    }

    public void setAddress(AddressDetailDTO address) {
        this.address = address;
    }

    public List<PhoneNumberDetailDTO> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(List<PhoneNumberDetailDTO> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public List<Long> getIdsContactGroup() {
        return idsContactGroup;
    }

    public void setIdsContactGroup(List<Long> idsContactGroup) {
        this.idsContactGroup = idsContactGroup;
    }
}
