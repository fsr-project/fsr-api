package controller.dto.detail.contact;

import controller.dto.common.CommonPhoneNumberDTO;

public class PhoneNumberDetailDTO extends CommonPhoneNumberDTO {
    private long idPhoneNumber;

    public PhoneNumberDetailDTO() {
    }

    public PhoneNumberDetailDTO(long idPhoneNumber, String phoneKind, String phoneNumber) {
        super(phoneKind, phoneNumber);
        this.idPhoneNumber = idPhoneNumber;
    }

    public long getIdPhoneNumber() {
        return idPhoneNumber;
    }

    public void setIdPhoneNumber(long idPhoneNumber) {
        this.idPhoneNumber = idPhoneNumber;
    }
}
