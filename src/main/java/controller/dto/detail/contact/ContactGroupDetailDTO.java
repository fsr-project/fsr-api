package controller.dto.detail.contact;

import controller.dto.common.CommonContactGroupDTO;

public class ContactGroupDetailDTO extends CommonContactGroupDTO {
    private long idContactGroup;

    public ContactGroupDetailDTO(long idContactGroup, String name) {
        super(name);
        this.idContactGroup = idContactGroup;
    }

    public long getIdContactGroup() {
        return idContactGroup;
    }

    public void setIdContactGroup(long idContactGroup) {
        this.idContactGroup = idContactGroup;
    }
}
