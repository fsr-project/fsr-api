package controller.dto.detail.group;

import controller.dto.common.CommonContactGroupDTO;

import java.util.List;

public class GroupUpdateDTO extends CommonContactGroupDTO {
    private long idGroup;

    private List<Long> idsContact;

    public GroupUpdateDTO() {
    }

    public long getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(long idGroup) {
        this.idGroup = idGroup;
    }

    public List<Long> getIdsContact() {
        return idsContact;
    }

    public void setIdsContact(List<Long> idsContact) {
        this.idsContact = idsContact;
    }
}
