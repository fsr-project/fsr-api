package controller.dto.detail.group;

import controller.dto.common.CommonContactGroupDTO;

import java.util.List;

public class GroupDetailDTO extends CommonContactGroupDTO {
    private long idGroup;

    private List<GroupContactDetailDTO> contacts;

    public GroupDetailDTO(long idGroup, String name, List<GroupContactDetailDTO> contacts) {
        super(name);
        this.idGroup = idGroup;
        this.contacts = contacts;
    }

    public long getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(long idGroup) {
        this.idGroup = idGroup;
    }

    public List<GroupContactDetailDTO> getContacts() {
        return contacts;
    }

    public void setContacts(List<GroupContactDetailDTO> contacts) {
        this.contacts = contacts;
    }
}
