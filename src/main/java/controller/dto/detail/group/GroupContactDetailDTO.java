package controller.dto.detail.group;

public class GroupContactDetailDTO {
    private long idContact;

    private String firstname;

    private String lastname;

    public GroupContactDetailDTO(long idContact, String firstname, String lastname) {
        this.idContact = idContact;
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public long getIdContact() {
        return idContact;
    }

    public void setIdContact(long idContact) {
        this.idContact = idContact;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}
