package controller.dto.creation;

import controller.dto.common.CommonContactGroupDTO;

import java.util.List;

public class GroupCreationDTO extends CommonContactGroupDTO {

    private List<Long> idsContact;


    public List<Long> getIdsContact() {
        return idsContact;
    }

    public void setIdsContact(List<Long> idsContact) {
        this.idsContact = idsContact;
    }
}
