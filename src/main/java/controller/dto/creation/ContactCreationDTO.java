package controller.dto.creation;

import controller.dto.common.CommonAddressDTO;
import controller.dto.common.CommonContactDTO;
import controller.dto.common.CommonPhoneNumberDTO;

import java.util.List;

public class ContactCreationDTO extends CommonContactDTO {

    private CommonAddressDTO address;

    private List<CommonPhoneNumberDTO> phoneNumbers;

    private List<Long> idsContactGroup;

    public ContactCreationDTO() {
    }

    public ContactCreationDTO(
            String firstName,
            String lastName,
            String email,
            CommonAddressDTO address,
            List<CommonPhoneNumberDTO> phoneNumbers,
            List<Long> idsContactGroup
    ) {
        super(firstName, lastName, email);
        this.address = address;
        this.phoneNumbers = phoneNumbers;
        this.idsContactGroup = idsContactGroup;
    }

    public CommonAddressDTO getAddress() {
        return address;
    }

    public void setAddress(CommonAddressDTO address) {
        this.address = address;
    }

    public List<CommonPhoneNumberDTO> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(List<CommonPhoneNumberDTO> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public List<Long> getIdsContactGroup() {
        return idsContactGroup;
    }

    public void setIdsContactGroup(List<Long> idsContactGroup) {
        this.idsContactGroup = idsContactGroup;
    }
}
