package services.mapper.input.group;

import controller.dto.creation.GroupCreationDTO;
import model.ContactGroup;
import org.springframework.stereotype.Component;
import services.mapper.IMapper;

@Component
public class GroupMapper implements IMapper<ContactGroup, GroupCreationDTO> {
    public GroupMapper() {
    }

    @Override
    public ContactGroup map(GroupCreationDTO entity) {
        ContactGroup group = new ContactGroup();
        group.setName(entity.getName());
        return group;
    }
}
