package services.mapper.input.contact;

import controller.dto.common.CommonAddressDTO;
import controller.dto.common.CommonPhoneNumberDTO;
import controller.dto.creation.ContactCreationDTO;
import daos.IDAO;
import model.Address;
import model.Contact;
import model.ContactGroup;
import model.PhoneNumber;
import org.springframework.stereotype.Component;
import services.mapper.IMapper;

@Component
public class ContactMapper implements IMapper<Contact, ContactCreationDTO> {
    private IDAO utilDAO;

    public ContactMapper(IDAO utilDAO) {
        this.utilDAO = utilDAO;
    }

    public ContactMapper() {
    }

    @Override
    public Contact map(ContactCreationDTO entity) {
        Contact contact = new Contact(
                entity.getFirstName(),
                entity.getLastName(),
                entity.getEmail(),
                toAddress(entity.getAddress())
        );

        entity.getPhoneNumbers().forEach(phoneDTO -> {
            contact.addPhoneNumber(this.toPhoneNumber(phoneDTO));
        });

        if (entity.getIdsContactGroup() != null && entity.getIdsContactGroup().size() > 0)
            entity.getIdsContactGroup().forEach(id -> {
                    contact.addGroup(utilDAO.getReference(ContactGroup.class, id));
            });

        return contact;
    }

    private Address toAddress(CommonAddressDTO addressDTO){
        return new Address(
              addressDTO.getStreet(),
              addressDTO.getCity(),
              addressDTO.getZip(),
              addressDTO.getCountry()
        );
    }

    private PhoneNumber toPhoneNumber(CommonPhoneNumberDTO phoneNumberDTO) {
        return new PhoneNumber(
                phoneNumberDTO.getPhoneKind(),
                phoneNumberDTO.getPhoneNumber()
        );
    }

}
