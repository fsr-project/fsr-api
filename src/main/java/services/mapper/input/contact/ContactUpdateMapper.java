package services.mapper.input.contact;

import controller.dto.detail.contact.AddressDetailDTO;
import controller.dto.detail.contact.ContactUpdateDTO;
import controller.dto.detail.contact.PhoneNumberDetailDTO;
import daos.IDAO;
import model.Address;
import model.Contact;
import model.ContactGroup;
import model.PhoneNumber;
import org.springframework.stereotype.Component;
import services.mapper.IMapper;

@Component
public class ContactUpdateMapper implements IMapper<Contact, ContactUpdateDTO> {

    private IDAO utilDAO;

    public ContactUpdateMapper(IDAO utilDAO) {
        this.utilDAO = utilDAO;
    }

    public ContactUpdateMapper() {
    }

    @Override
    public Contact map(ContactUpdateDTO entity) {
        Contact contact = new Contact(
                entity.getFirstName(),
                entity.getLastName(),
                entity.getEmail(),
                toAddress(entity.getAddress())
        );

        contact.setIdContact(entity.getIdContact());

        entity.getPhoneNumbers().forEach(phoneDTO -> {
            contact.addPhoneNumber(this.toPhoneNumber(phoneDTO));
        });

        if (entity.getIdsContactGroup() != null && entity.getIdsContactGroup().size() > 0)
            entity.getIdsContactGroup().forEach(id -> {
                contact.addGroup(utilDAO.getReference(ContactGroup.class, id));
            });

        return contact;
    }

    private Address toAddress(AddressDetailDTO addressDTO){
        return new Address(
                addressDTO.getIdAddress(),
                addressDTO.getStreet(),
                addressDTO.getCity(),
                addressDTO.getZip(),
                addressDTO.getCountry()
        );
    }

    private PhoneNumber toPhoneNumber(PhoneNumberDetailDTO phoneNumberDTO) {
        return new PhoneNumber(
                phoneNumberDTO.getIdPhoneNumber(),
                phoneNumberDTO.getPhoneKind(),
                phoneNumberDTO.getPhoneNumber()
        );
    }
}
