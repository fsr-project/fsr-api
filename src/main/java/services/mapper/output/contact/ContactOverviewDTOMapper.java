package services.mapper.output.contact;

import controller.dto.overview.ContactOverviewDTO;
import services.mapper.IMapper;
import model.Contact;
import org.springframework.stereotype.Component;

@Component
public class ContactOverviewDTOMapper implements IMapper<ContactOverviewDTO, Contact> {
    @Override
    public ContactOverviewDTO map(Contact entity) {
        return new ContactOverviewDTO(
                entity.getIdContact(),
                entity.getFirstName(),
                entity.getLastName(),
                entity.getEmail()
        );
    }
}
