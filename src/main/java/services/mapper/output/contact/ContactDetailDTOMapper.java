package services.mapper.output.contact;

import controller.dto.detail.contact.AddressDetailDTO;
import controller.dto.detail.contact.ContactDetailDTO;
import controller.dto.detail.contact.ContactGroupDetailDTO;
import controller.dto.detail.contact.PhoneNumberDetailDTO;
import model.Address;
import model.Contact;
import model.ContactGroup;
import model.PhoneNumber;
import org.springframework.stereotype.Component;
import services.mapper.IMapper;

import java.util.stream.Collectors;

@Component
public class ContactDetailDTOMapper implements IMapper<ContactDetailDTO, Contact> {
    @Override
    public ContactDetailDTO map(Contact entity) {
        return new ContactDetailDTO(
                entity.getIdContact(),
                entity.getFirstName(),
                entity.getLastName(),
                entity.getEmail(),
                toAddressDetail(entity.getAddress()),
                entity.getPhoneNumbers().stream().map(this::toPhoneNumberDetail).collect(Collectors.toList()),
                entity.getGroups().stream().map(this::toContactGroupDetail).collect(Collectors.toList())
        );
    }

    private AddressDetailDTO toAddressDetail(Address address){
         return new AddressDetailDTO(
                 address.getIdAddress(),
                 address.getStreet(),
                 address.getCity(),
                 address.getZip(),
                 address.getCountry()
         );
    }

    private PhoneNumberDetailDTO toPhoneNumberDetail(PhoneNumber phoneNumber){
        return new PhoneNumberDetailDTO(
                phoneNumber.getIdPhoneNumber(),
                phoneNumber.getPhoneKind(),
                phoneNumber.getPhoneNumber()
        );
    }

    private ContactGroupDetailDTO toContactGroupDetail(ContactGroup group){
        return new ContactGroupDetailDTO(
                group.getIdContactGroup(),
                group.getName()
        );
    }
}
