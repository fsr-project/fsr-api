package services.mapper.output.group;

import controller.dto.detail.group.GroupContactDetailDTO;
import controller.dto.detail.group.GroupDetailDTO;
import model.Contact;
import model.ContactGroup;
import services.mapper.IMapper;

import java.util.stream.Collectors;

public class GroupDetailDTOMapper implements IMapper<GroupDetailDTO, ContactGroup> {
    @Override
    public GroupDetailDTO map(ContactGroup entity) {
        return new GroupDetailDTO(
                entity.getIdContactGroup(),
                entity.getName(),
                entity.getContacts()
                        .stream()
                        .map(this::toContact)
                        .collect(Collectors.toList())
        );
    }

    private GroupContactDetailDTO toContact(Contact contact) {
        return new GroupContactDetailDTO(
                contact.getIdContact(),
                contact.getFirstName(),
                contact.getLastName()
        );
    }
}
