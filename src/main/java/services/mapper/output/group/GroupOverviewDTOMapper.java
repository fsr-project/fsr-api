package services.mapper.output.group;

import controller.dto.overview.GroupOverviewDTO;
import model.ContactGroup;
import org.springframework.stereotype.Component;
import services.mapper.IMapper;

@Component
public class GroupOverviewDTOMapper implements IMapper<GroupOverviewDTO, ContactGroup> {
    @Override
    public GroupOverviewDTO map(ContactGroup entity) {
        return new GroupOverviewDTO(
                entity.getIdContactGroup(),
                entity.getName(),
                entity.getContacts().size()
        );
    }
}
