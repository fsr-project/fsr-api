package services.mapper;

public interface IMapper<T, A> {
    T map(A entity);
}
