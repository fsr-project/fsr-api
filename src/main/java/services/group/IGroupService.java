package services.group;

import controller.dto.creation.GroupCreationDTO;
import controller.dto.detail.group.GroupDetailDTO;
import controller.dto.detail.group.GroupUpdateDTO;
import controller.dto.overview.GroupOverviewDTO;
import daos.exceptions.GroupCreationException;
import daos.exceptions.GroupNotFoundException;

import java.util.List;

public interface IGroupService {
    List<GroupOverviewDTO> getAllGroups();

    void createGroup(GroupCreationDTO group) throws GroupCreationException;

    GroupDetailDTO getGroupById(long id) throws GroupNotFoundException;

    void deleteGroup(long id) throws GroupNotFoundException;

    void updateGroup(GroupUpdateDTO group) throws GroupNotFoundException;
}
