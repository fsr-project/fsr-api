package services.group;

import controller.dto.creation.GroupCreationDTO;
import controller.dto.detail.group.GroupDetailDTO;
import controller.dto.detail.group.GroupUpdateDTO;
import controller.dto.overview.GroupOverviewDTO;
import daos.exceptions.GroupCreationException;
import daos.exceptions.GroupNotFoundException;
import daos.group.IGroupDAO;
import model.ContactGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import services.mapper.IMapper;
import services.mapper.input.group.GroupMapper;
import services.mapper.output.group.GroupDetailDTOMapper;
import services.mapper.output.group.GroupOverviewDTOMapper;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class GroupService implements IGroupService{

    @Autowired
    private IGroupDAO dao;

    public GroupService() {
    }

    @Override
    public List<GroupOverviewDTO> getAllGroups() {
        IMapper<GroupOverviewDTO, ContactGroup> mapper = new GroupOverviewDTOMapper();
        return dao.getAllGroups()
                .stream()
                .map(mapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public void createGroup(GroupCreationDTO group) throws GroupCreationException {
        IMapper<ContactGroup, GroupCreationDTO> mapper = new GroupMapper();
        dao.createGroup(mapper.map(group), group.getIdsContact());
    }

    @Override
    public GroupDetailDTO getGroupById(long id) throws GroupNotFoundException {
        IMapper<GroupDetailDTO, ContactGroup> mapper = new GroupDetailDTOMapper();
        return mapper.map(dao.getGroupById(id));
    }

    @Override
    public void deleteGroup(long id) throws GroupNotFoundException {
        dao.deleteGroup(id);
    }

    @Override
    public void updateGroup(GroupUpdateDTO group) throws GroupNotFoundException {
        dao.updateGroup(
                new ContactGroup(
                    group.getIdGroup(),
                    group.getName()
                ),
                group.getIdsContact()
        );
    }
}
