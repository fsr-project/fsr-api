package services.contact;

import controller.dto.creation.ContactCreationDTO;
import controller.dto.detail.contact.ContactDetailDTO;
import controller.dto.detail.contact.ContactUpdateDTO;
import controller.dto.overview.ContactOverviewDTO;
import daos.IDAO;
import daos.UtilsDAO;
import daos.contact.IContactDAO;
import daos.exceptions.ContactCreationException;
import daos.exceptions.ContactNotFoundException;
import model.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import services.mapper.IMapper;
import services.mapper.input.contact.ContactMapper;
import services.mapper.input.contact.ContactUpdateMapper;
import services.mapper.output.contact.ContactDetailDTOMapper;
import services.mapper.output.contact.ContactOverviewDTOMapper;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ContactService implements IContactService{
    @Autowired
    private IContactDAO dao;

    private final IDAO utilsDAO;

    public ContactService() {
        this.utilsDAO = UtilsDAO.getInstance();
    }

    public List<ContactOverviewDTO> getContacts() {
        IMapper<ContactOverviewDTO, Contact> mapper = new ContactOverviewDTOMapper();
        return dao.getAllContacts()
                .stream()
                .map(mapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public void createContact(ContactCreationDTO contact) throws ContactCreationException {
        IMapper<Contact, ContactCreationDTO> mapper = new ContactMapper(utilsDAO);
        dao.createContact(mapper.map(contact));
    }

    @Override
    public Optional<ContactDetailDTO> getContactById(long id) throws ContactNotFoundException {
        IMapper<ContactDetailDTO, Contact> mapper = new ContactDetailDTOMapper();
        return Optional.of(mapper.map(dao.getContactByID(id)));
    }

    @Override
    public void deleteContact(long id) throws ContactNotFoundException {
        dao.deleteContact(id);
    }

    @Override
    public void updateContact(ContactUpdateDTO contact) throws ContactNotFoundException {
        IMapper<Contact, ContactUpdateDTO> mapper = new ContactUpdateMapper(utilsDAO);
        dao.updateContact(mapper.map(contact));
    }
}
