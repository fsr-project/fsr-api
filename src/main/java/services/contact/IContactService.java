package services.contact;

import controller.dto.creation.ContactCreationDTO;
import controller.dto.detail.contact.ContactDetailDTO;
import controller.dto.detail.contact.ContactUpdateDTO;
import controller.dto.overview.ContactOverviewDTO;
import daos.exceptions.ContactCreationException;
import daos.exceptions.ContactNotFoundException;

import java.util.List;
import java.util.Optional;

public interface IContactService {
    List<ContactOverviewDTO> getContacts();

    void createContact(ContactCreationDTO contact) throws ContactCreationException;

    Optional<ContactDetailDTO> getContactById(long id) throws ContactNotFoundException;

    void deleteContact(long id) throws ContactNotFoundException;

    void updateContact(ContactUpdateDTO contact) throws ContactNotFoundException;
}
