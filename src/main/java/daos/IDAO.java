package daos;

public interface IDAO {
    <T> T getReference(Class<T> entity, long id);
}
