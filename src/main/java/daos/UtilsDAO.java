package daos;

import daos.exceptions.ReferenceNotFoudException;
import util.JpaUtil;

import javax.persistence.EntityManager;

public class UtilsDAO implements IDAO {
    private static UtilsDAO instance;

    private UtilsDAO() {}

    public static UtilsDAO getInstance() {
        if (instance == null) {
            instance = new UtilsDAO();
        }
        return instance;
    }

    @Override
    public <T> T getReference(Class<T> entity, long id) {
        EntityManager entityManager = JpaUtil.getEmf().createEntityManager();
        try {
            entityManager.getTransaction().begin();
            T reference = entityManager.getReference(entity, id);
            entityManager.getTransaction().commit();
            if (reference == null)
                throw new ReferenceNotFoudException("Reference not found for id " + id);
            return reference;
        } catch (Exception e) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            throw new RuntimeException("Error getting reference for entity " + entity + " and id : " + id);
        } finally {
            entityManager.close();
        }
    }
}

