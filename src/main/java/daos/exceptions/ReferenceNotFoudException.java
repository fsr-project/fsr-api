package daos.exceptions;

public class ReferenceNotFoudException extends Exception{
    public ReferenceNotFoudException(String message) {
        super(message);
    }
}
