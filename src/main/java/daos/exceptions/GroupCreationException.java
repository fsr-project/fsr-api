package daos.exceptions;

public class GroupCreationException extends Exception{
    public GroupCreationException(String message) {
        super(message);
    }
}
