package daos.exceptions;

public class ContactCreationException extends Exception{
    public ContactCreationException(String message) {
        super(message);
    }
}
