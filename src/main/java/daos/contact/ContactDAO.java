package daos.contact;

import daos.exceptions.ContactCreationException;
import daos.exceptions.ContactNotFoundException;
import model.Contact;
import org.springframework.stereotype.Repository;
import util.JpaUtil;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository("contactDAO")
public class ContactDAO implements IContactDAO {

    public List<Contact> getAllContacts() {
        EntityManager entityManager = JpaUtil.getEmf().createEntityManager();
        try {
            entityManager.getTransaction().begin();

            TypedQuery<Contact> query = entityManager.createQuery("SELECT c FROM Contact c", Contact.class);
            List<Contact> contacts = query.getResultList();

            entityManager.getTransaction().commit();

            return contacts;
        } catch (Exception e) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            throw new RuntimeException("Error getting all contacts", e);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void createContact(Contact contact) throws ContactCreationException {
        EntityManager entityManager = JpaUtil.getEmf().createEntityManager();
        try {
            if (isInfoContactNotNull(contact)){
                entityManager.getTransaction().begin();
                entityManager.persist(contact);
                entityManager.getTransaction().commit();
            }
            else {
                throw new ContactCreationException("This contact have not enough information to allow his creation !");
            }
        } catch (ContactCreationException e) {
            throw e;
        }
        catch (Exception e) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            throw new RuntimeException("Error adding a new contact", e);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public Contact getContactByID(long id) throws ContactNotFoundException {
        EntityManager entityManager = JpaUtil.getEmf().createEntityManager();
        try {
            entityManager.getTransaction().begin();
            Contact contact = entityManager.createNamedQuery("Contact.getContactById", Contact.class)
                    .setParameter("idParam", id)
                    .getSingleResult();
            entityManager.getTransaction().commit();
            if (contact != null) {
                contact.getPhoneNumbers().size();
                contact.getGroups().size();
            }

            return contact;
        } catch (NoResultException exception){
            throw new ContactNotFoundException("Contact with ID " + id + " not found");
        }
        catch (Exception e) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            throw new RuntimeException("Error getting contact by id", e);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void deleteContact(long id) throws ContactNotFoundException {
        EntityManager entityManager = JpaUtil.getEmf().createEntityManager();
        try {
            entityManager.getTransaction().begin();
            Contact contact = entityManager.find(Contact.class, id);
            if (contact != null){
                entityManager.remove(contact);
            }
            else {
                throw new ContactNotFoundException("Contact with ID " + id + " not found");
            }
            entityManager.getTransaction().commit();
        }
        catch (ContactNotFoundException exception){
            throw exception;
        }
        catch (Exception e) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            throw new RuntimeException("Error deleting the contact", e);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateContact(Contact contact) throws ContactNotFoundException {
        EntityManager entityManager = JpaUtil.getEmf().createEntityManager();
        try {
            entityManager.getTransaction().begin();
            Contact contactRef = entityManager.find(Contact.class, contact.getIdContact());
            if (contactRef != null){
                entityManager.merge(contact);
            }
            else {
                throw new ContactNotFoundException("Contact with ID " + contact.getIdContact() + " not found");
            }
            entityManager.getTransaction().commit();
        }
        catch (ContactNotFoundException exception){
            throw exception;
        }
        catch (Exception e) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            throw new RuntimeException("Error deleting the contact", e);
        } finally {
            entityManager.close();
        }
    }

    private boolean isInfoContactNotNull(Contact contact){
        return (
                contact != null
                && contact.getFirstName() != null
                && contact.getLastName() != null
                && contact.getEmail() != null
        );
    }
}
