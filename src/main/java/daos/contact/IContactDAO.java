package daos.contact;

import daos.exceptions.ContactCreationException;
import daos.exceptions.ContactNotFoundException;
import model.Contact;

import java.util.List;

public interface IContactDAO {
    List<Contact> getAllContacts();

    void createContact(Contact contact) throws ContactCreationException;

    Contact getContactByID(long id) throws ContactNotFoundException;

    void deleteContact(long id) throws ContactNotFoundException;

    void updateContact(Contact contact) throws ContactNotFoundException;
}
