package daos.group;

import daos.exceptions.GroupCreationException;
import daos.exceptions.GroupNotFoundException;
import model.Contact;
import model.ContactGroup;
import org.springframework.stereotype.Repository;
import util.JpaUtil;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository("groupDAO")
public class GroupDAO implements IGroupDAO{
    @Override
    public List<ContactGroup> getAllGroups() {
        EntityManager entityManager = JpaUtil.getEmf().createEntityManager();
        try {
            entityManager.getTransaction().begin();

            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<ContactGroup> cq = cb.createQuery(ContactGroup.class);
            Root<ContactGroup> rootEntry = cq.from(ContactGroup.class);
            CriteriaQuery<ContactGroup> all = cq.select(rootEntry);
            List<ContactGroup> groups = entityManager.createQuery(all).getResultList();

            entityManager.getTransaction().commit();

            groups.forEach(group -> group.getContacts().size());

            return groups;
        } catch (Exception e) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            throw new RuntimeException("Error getting all the groups", e);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void createGroup(ContactGroup group, List<Long> idsContact) throws GroupCreationException {
        EntityManager entityManager = JpaUtil.getEmf().createEntityManager();
        try {
            if (group.getName() != null && group.getName().trim().length() > 0){
                entityManager.getTransaction().begin();
                entityManager.persist(group);
                if (idsContact != null) {
                    idsContact.forEach(id -> {
                        Contact contact = entityManager.find(Contact.class, id);
                        if (contact != null) {
                            contact.addGroup(group);
                        }
                    });
                }
                entityManager.getTransaction().commit();
            }
            else {
                throw new GroupCreationException("Exception, This group does not have a name ! GroupName = " + group.getName());
            }
        } catch (GroupCreationException e) {
            throw e;
        }
        catch (Exception e) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            throw new RuntimeException("Error adding a new group");
        } finally {
            entityManager.close();
        }
    }

    @Override
    public ContactGroup getGroupById(long id) throws GroupNotFoundException {
        EntityManager entityManager = JpaUtil.getEmf().createEntityManager();
        try {
            entityManager.getTransaction().begin();
            ContactGroup group = entityManager.find(ContactGroup.class, id);
            entityManager.getTransaction().commit();
            if (group != null) {
                group.getContacts().size();
            }
            else {
                throw new GroupNotFoundException("Group with ID " + id + " not found");
            }
            return group;
        } catch (GroupNotFoundException exception){
            throw exception;
        }
        catch (Exception e) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            throw new RuntimeException("Error getting group by id", e);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void deleteGroup(long id) throws GroupNotFoundException {
        EntityManager entityManager = JpaUtil.getEmf().createEntityManager();
        try {
            entityManager.getTransaction().begin();
            ContactGroup group = entityManager.find(ContactGroup.class, id);
            if (group != null){
                List<Contact> contacts = group.getContacts();
                if(contacts != null && contacts.size() > 0) {
                    contacts.forEach(contact -> {
                        contact.removeGroup(group);
                    });
                }
                entityManager.remove(group);
            }
            else {
                throw new GroupNotFoundException("Group with ID " + id + " not found");
            }
            entityManager.getTransaction().commit();
        }
        catch (GroupNotFoundException exception){
            throw exception;
        }
        catch (Exception e) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            throw new RuntimeException("Error deleting the group", e);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateGroup(ContactGroup group, List<Long> idsContact) throws GroupNotFoundException {
        EntityManager entityManager = JpaUtil.getEmf().createEntityManager();
        try {
            entityManager.getTransaction().begin();
            ContactGroup groupRef = entityManager.find(ContactGroup.class, group.getIdContactGroup());
            if (groupRef != null){
                List<Contact> oldContacts = groupRef.getContacts();
                if (oldContacts != null)
                    oldContacts.forEach(oldContact -> {
                        oldContact.removeGroup(groupRef);
                    });

                entityManager.merge(group);
                if (idsContact != null)
                    idsContact.forEach(id -> {
                        Contact contact = entityManager.find(Contact.class, id);
                        if (contact != null)
                            contact.addGroup(group);
                    });

            }
            else {
                throw new GroupNotFoundException("Group with ID " + group.getIdContactGroup() + " not found");
            }
            entityManager.getTransaction().commit();
        }
        catch (GroupNotFoundException exception){
            throw exception;
        }
        catch (Exception e) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            throw new RuntimeException("Error updating the group", e);
        } finally {
            entityManager.close();
        }
    }
}
