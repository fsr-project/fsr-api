package daos.group;

import daos.exceptions.GroupCreationException;
import daos.exceptions.GroupNotFoundException;
import model.ContactGroup;

import java.util.List;

public interface IGroupDAO {
    List<ContactGroup> getAllGroups();

    void createGroup (ContactGroup group, List<Long> idsContact) throws GroupCreationException;

    ContactGroup getGroupById(long id) throws GroupNotFoundException;

    void deleteGroup(long id) throws GroupNotFoundException;

    void updateGroup(ContactGroup group, List<Long> idsContact) throws GroupNotFoundException;
}
